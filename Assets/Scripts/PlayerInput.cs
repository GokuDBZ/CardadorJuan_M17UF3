using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Cinemachine;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;
using static UnityEditor.Progress;
public class PlayerInput : MonoBehaviour
{
    //input fields
    private Controls controls;
    Vector2 mouseInput;
    [SerializeField] float sensitivityX = 8f;
    [SerializeField] float sensitivityY = 0.5f;
    float mouseX, mouseY;

    //movement fields
    private Rigidbody rb;
    //[SerializeField] private float movementForce = 1f;
    [SerializeField] private float jumpForce = 5f;
    [SerializeField] private float speed = 5f;
    private Vector3 forceDirection = Vector3.zero;
    private Vector2 moveInput;
    [SerializeField] private CinemachineVirtualCamera virtualCamera;
    private Camera playerCamera;
    private Animator animator;

    //booleans
    //private bool dead = false; para el futuro
    private bool crouched = false;
    private bool aiming = false;
    private bool dancing = false;
    private bool walking = false;

    public List<GameItems> inventory = new List<GameItems>();
    //private List<Collider> collisions;
    [System.Serializable]
    public class GameItems 
    {
        public GameObject reference;
        public string name;

        public void Equip() 
        {
            reference.SetActive(true);
        }
    }
    private void Awake()
    {
        rb = this.GetComponent<Rigidbody>();
        controls = new Controls();
        animator = this.GetComponent<Animator>();
    }

    private void OnEnable()
    {
        //Asigna las funciones al respectivo control (osease asigna la funcion pertinente a cada tecla) Ejemplo: asigna funcion salto al control salto (Espacio)
        controls.Player.Jump.started += DoJump;
        //if (inventory.Find(x => x.name == "Bow").reference.activeSelf)
        //{
            
        //}
        
        controls.Player.Shoot.started += DoShoot; 
        controls.Player.Dance.started += DoDance;
        controls.Player.Aim.started += DoAim;
        controls.Player.Crouch.started += DoCrouch;
        controls.Player.Walk.started += DoWalk;
        controls.Player.Enable();
    }

    private void OnDisable()
    {
        controls.Player.Disable();
    }

    void FixedUpdate()
    {
        //Movimiento + salto
        moveInput = controls.Player.Move.ReadValue<Vector2>() * speed;

        Vector3 moveInput3D = new Vector3(moveInput.x, 0, moveInput.y); // make it into a vec3
        Vector3 worldMove = Camera.main.transform.TransformVector(moveInput3D); // convert it to world-space
        Vector3 move = Vector3.ProjectOnPlane(worldMove, Vector3.up); // get rid of any resulting vertical component
        move *= worldMove.magnitude / move.magnitude; // compensate for the reduction in length
        move.y = rb.velocity.y;
        if (controls.Player.Move.ReadValue<Vector2>().x != 0 || controls.Player.Move.ReadValue<Vector2>().y != 0)
        {
            rb.velocity = move;
        } 

        rb.AddForce(forceDirection, ForceMode.Impulse);
        forceDirection = Vector3.zero;
        animator.SetBool("Falling", !IsGrounded());
        animator.SetBool("Jumping", false);

        //Rotacion personaje en base a donde mira la camara
        if (dancing == false) transform.rotation = new Quaternion(transform.rotation.x, Camera.main.transform.rotation.y, transform.rotation.z, transform.rotation.w);
        }

    void Update()
    {
        animator.SetFloat("Front Move", controls.Player.Move.ReadValue<Vector2>().y);
        animator.SetFloat("Side Move", controls.Player.Move.ReadValue<Vector2>().x);
        if (UnityEngine.Input.GetKeyDown(KeyCode.Z)) Death();        
    }

    private void DoJump(InputAction.CallbackContext obj)
    {
        if (IsGrounded())
        {
            forceDirection += Vector3.up * jumpForce;
            animator.SetBool("Jumping", true);
        }
    }

    private bool IsGrounded()
    {
        return Physics.Raycast(this.transform.position, Vector3.down, out RaycastHit hit, 0.028f);
    }
    private void DoDance(InputAction.CallbackContext obj)
    {
        if (dancing == false)
        {
            dancing = true;
            animator.SetBool("Dancing", true);
            GameObject.Find("DanceCamera").GetComponent<CinemachineVirtualCamera>().Priority = 12;
            speed = 0;
            controls.Player.Jump.started -= DoJump;
            controls.Player.Shoot.started -= DoShoot;
            controls.Player.Aim.started -= DoAim;
            controls.Player.Crouch.started -= DoCrouch;
        }
        else
        {
            dancing = false;
            animator.SetBool("Dancing", false);
            GameObject.Find("DanceCamera").GetComponent<CinemachineVirtualCamera>().Priority = 10;
            speed = 5;
            controls.Player.Jump.started += DoJump;
            controls.Player.Shoot.started += DoShoot;
            controls.Player.Aim.started += DoAim;
            controls.Player.Crouch.started += DoCrouch;
        }           
    }
    private void DoAim(InputAction.CallbackContext obj)
    {
        if (FindItem("Bow").reference.activeSelf)
        {
            if (aiming == false)
            {
                aiming = true;
                animator.SetBool("Aiming", true);
                controls.Player.Jump.started -= DoJump;
                controls.Player.Dance.started -= DoDance;
                GameObject.Find("AimCamera").GetComponent<CinemachineFreeLook>().Priority = 12;
                SoundManager.instance.Play("Aim");
            }
            else
            {
                aiming = false;
                animator.SetBool("Aiming", false);
                controls.Player.Jump.started += DoJump;
                controls.Player.Dance.started += DoDance;
                GameObject.Find("AimCamera").GetComponent<CinemachineFreeLook>().Priority = 9;
            }
        }        
    }
    private void DoCrouch(InputAction.CallbackContext obj)
    {
        if (crouched == false)
        {
            crouched = true;
            speed = 3f;
            animator.SetBool("Crouch", true);
            controls.Player.Jump.started -= DoJump;
            controls.Player.Dance.started -= DoDance;
        }
        else 
        {
            crouched = false;
            speed = 5f;
            animator.SetBool("Crouch", false);
            if (aiming == false) 
            { 
                controls.Player.Jump.started += DoJump;
                controls.Player.Dance.started += DoDance;
            }
        }
        
    }
    private void DoWalk(InputAction.CallbackContext obj)
    {
        if (walking == false)
        {
            walking = true;
            speed = 1.5f;
            animator.SetBool("Walking", true);
        }
        else
        {
            walking = false;
            speed = 5f;
            animator.SetBool("Walking", false);
        }

    }
    private void DoShoot(InputAction.CallbackContext obj)
    {
        if (FindItem("Bow").reference.activeSelf)
        {
            if (aiming == true)
            {
                animator.SetTrigger("Shoot");
                SoundManager.instance.Play("Shoot");
            }
        }
    }
    private void Death() 
    {
        //dead = true; para el futuro
        animator.SetBool("Dead", true);
        controls.Player.Disable();
    }
    public void RecieveInput(Vector2 mouseInput)
    {
        mouseX = mouseInput.x * sensitivityX;
        mouseY = mouseInput.y * sensitivityY;
    }
    //Iluminar, recoger objetos
    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Pickable"))
        {
            GameObject item = other.gameObject;
            other.GetComponent<Highlight>().ToggleHighlight(true);
            if (controls.Player.Interact.IsInProgress())
            {
                FindItem(item.name).Equip();
                SoundManager.instance.Play("PickUp");
                //inventory.Find(x => x.name == item.name).Equip();
                item.SetActive(false);

            }
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Pickable"))
        {
            other.GetComponent<Highlight>().ToggleHighlight(false);
        }
    }
    public GameItems FindItem(string name) 
    {
        return inventory.Find(x => x.name == name);
    }
}

