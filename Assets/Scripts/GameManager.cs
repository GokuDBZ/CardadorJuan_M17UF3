using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using static UnityEditor.Progress;


public class GameManager : MonoBehaviour
{

    [HideInInspector]
    public static GameManager Instance;

    //bools
    bool[] items;

    [Serializable]
    public class SaveData
    {
        public bool item1;
        public bool item2;
        public bool item3;
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else if (Instance != this) Destroy(gameObject);

        Load();
        Debug.Log(Application.persistentDataPath + "/Data.json");
    }
    void PickObject(GameObject item) 
    {

    }



    private void Load()
    {
        
        SaveData savedata = new SaveData();
        if (System.IO.File.Exists((Application.persistentDataPath + "/Data.json")))
        {            
            savedata.item1 = false;
            savedata.item2 = false;
            savedata.item3 = false;
        }
        else
        {
            string savefile = System.IO.File.ReadAllText(Application.persistentDataPath + "/Data.json");
            JsonUtility.FromJsonOverwrite(savefile, savedata);        
            Debug.Log(savedata); //comprobar que funciona
        }

        string itemsData = JsonUtility.ToJson(savedata);
        System.IO.File.WriteAllText(Application.persistentDataPath + "/Data.json", itemsData);
    }
    private void Save()
    {
        string itemsData = JsonUtility.ToJson(items);
        System.IO.File.WriteAllText(Application.persistentDataPath + "/Data.json", itemsData);
    }
}